### Tic Tac Nole: ###

**Minimum System Requirements:** 

* Android 4.0.3 IceCreamSandwich (API 15)
* Any RAM
* 6 MB disk space

**Recommended System Requirements:**

* Android 5.1 Lollipop (API 22)
* Any RAM
* 6 MB disk space

**Build Tools:**

The project was built with Android Studio using Build Tools Version 22.0.1, but was also successfully tested with Build Tools Version 23.0.0 rc2.

**Widgets:**

No custom widgets are needed, because only default Android widgets were used. 

**Widgets used:**

* TextView
* ImageButton
* Button
* RelativeLayout
* TableLayout
* TableRow