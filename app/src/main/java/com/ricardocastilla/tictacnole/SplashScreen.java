package com.ricardocastilla.tictacnole;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageButton;
import android.net.Uri;

public class SplashScreen extends Activity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        new Handler().postDelayed(new Runnable() {
            // Using handler with postDelayed called runnable run method

            @Override
            public void run() {
                Intent i = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(i);

                // close splash
                finish();
            }
        }, 3*1000); // wait for 5 seconds

        // attach a handler to the image to open cs's webpage
        ImageButton csBtn = (ImageButton) findViewById(R.id.cs_ad);
        csBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToCs(v);
            } // onClick
        });

    } // onCreate

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    } // onDestroy

    @Override
    protected void onPause()
    {
        super.onPause();
        finish();
    } //onPause

    // some helpful functions
    public void goToCs (View view) { goToUrl( "http://www.cs.fsu.edu/" ); } //goToCs

    private void goToUrl (String url)
    {
        Uri uri = Uri.parse(url);
        startActivity(new Intent(Intent.ACTION_VIEW, uri));
    } //goToUrl

} // SplashScreen
