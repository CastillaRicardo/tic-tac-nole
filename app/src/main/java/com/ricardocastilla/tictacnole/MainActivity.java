package com.ricardocastilla.tictacnole;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends Activity
    implements View.OnClickListener {

    public ImageButton[] cells; // declare cells
    private int plays = 0; // counts how many plays have been done
    private TextView information_view;
    private boolean gameOver;
    private boolean xTurn;
    private int xScore = 0, oScore = 0; // lets keep track of the best player

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button replayBtn;
        replayBtn = (Button) findViewById(R.id.replayBtn);
        replayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // reset the game
                resetGame();
            } // onClick
        });

        // cells to be used
        cells = new ImageButton[ 9 ]; // initialize cells
        cells[0] = (ImageButton) findViewById(R.id.topLeftBox);
        cells[1] = (ImageButton) findViewById(R.id.topMiddleBox);
        cells[2] = (ImageButton) findViewById(R.id.topRightBox);
        cells[3] = (ImageButton) findViewById(R.id.middleLeftBox);
        cells[4] = (ImageButton) findViewById(R.id.middleMiddleBox);
        cells[5] = (ImageButton) findViewById(R.id.middleRightBox);
        cells[6] = (ImageButton) findViewById(R.id.bottomLeftBox);
        cells[7] = (ImageButton) findViewById(R.id.bottomMiddleBox);
        cells[8] = (ImageButton) findViewById(R.id.bottomRightBox);
        for (int i = 0; i < 9; i++) {
            cells[i].setBackgroundResource(R.drawable.none);
            cells[i].setOnClickListener(this);
            cells[i].setTag(""+i); // ""+i to make them unique
        } // for
        Log.d("initialization", "cells initialized");
        information_view = (TextView) findViewById(R.id.information);
        gameOver = false; // game is not over at start
        xTurn = true; // x starts the game
        checkWinner();
    } // onCreate

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    } // onCreateOptionsMenu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.new_game) {
            resetGame();
            return true;
        } else if ( id == R.id.score_reset ) {
            xScore = 0;
            oScore = 0;
            TextView xScoreView = (TextView) findViewById(R.id.xScore);
            TextView oScoreView = (TextView) findViewById(R.id.oScore);
            xScoreView.setText("X Score: "+ xScore);
            oScoreView.setText("O Score: "+ oScore);
            return true;
        } // else if

        return super.onOptionsItemSelected(item);
    } // onOptionsItemSelected

    @Override
    public void onClick(View v) {
        // set the correct tag, image, and disable the button
        v.setBackgroundResource((xTurn) ? R.drawable.x : R.drawable.o);
        v.setTag(((xTurn) ? "x" : "o"));
        v.setClickable(false);
        xTurn = !xTurn; // revert xTurn
        information_view.setText( (xTurn)?R.string.x_turn:R.string.o_turn ); // change turn text
        plays++;
        Log.d("value", "plays:" + plays);
        int win_val = checkWinner();
        if ( win_val != 0 ) {
            endGame();
            showGameResetDialog(win_val);
        } // if
    } // onClick

    /* checkWinner
    * parameters: none
    * returns: int
    *   0-no winner
    *   1-player x won
    *   2-player o won
    *   3-draw
    */
    public int checkWinner() {
        // obtain TextViews
        TextView xScoreView = (TextView) findViewById(R.id.xScore);
        TextView oScoreView = (TextView) findViewById(R.id.oScore);

        Log.d("checks", "Checking for winner");
        // check rows for matches
        for (int i = 0; i < 9 && !gameOver; i+=3) {
            if (cells[i].getTag().equals(cells[i+1].getTag())
                    && cells[i].getTag().equals(cells[i+2].getTag()) ) {
                // check what the tag is
                if ( cells[i].getTag().equals("x") ) {
                    information_view.setText(R.string.x_won);
                    xScoreView.setText("X Score: "+ ++xScore);
                    Log.d("checks", "x won: Horizontal");
                    return 1; // player x has won
                } else {
                    information_view.setText(R.string.o_won);
                    oScoreView.setText("O Score: "+ ++oScore);;
                    Log.d("checks", "o won: Horizontal");
                    return 2;
                } // else
            } // if
        } // for
        // check vertical lines
        for (int i = 0; i < 3 && !gameOver; i++) {
            if (cells[i].getTag().equals(cells[i+3].getTag())
                    && cells[i].getTag().equals(cells[i+6].getTag()) ) {
                // check what the tag is
                if ( cells[i].getTag().equals("x") ) {
                    information_view.setText(R.string.x_won);
                    xScoreView.setText("X Score: "+ ++xScore);
                    Log.d("checks", "x won: Vertical");
                    return 1; // player x has won
                } else {
                    information_view.setText(R.string.o_won);
                    oScoreView.setText("O Score: "+ ++oScore);
                    Log.d("checks", "o won: Vertical");
                    return 2;
                } // else
            } // if
        } // for
        // check for diagonal win
        if (cells[0].getTag().equals(cells[4].getTag())
                && cells[0].getTag().equals(cells[8].getTag())) {
            if ( cells[0].getTag().equals("x") ) {
                Log.d("checks", "x won: Diagonal");
                information_view.setText(R.string.x_won);
                xScoreView.setText("X Score: "+ ++xScore);
                return 1; // player x has won
            } else {
                Log.d("checks", "o won: Diagonal");
                information_view.setText(R.string.o_won);
                oScoreView.setText("O Score: "+ ++oScore);;
                return 2;
            } // else
        } else if (cells[2].getTag().equals(cells[4].getTag())
                && cells[2].getTag().equals(cells[6].getTag()) ) {
            if ( cells[2].getTag().equals("x") ) {
                Log.d("checks", "x won: Diagonal");
                information_view.setText(R.string.x_won);
                xScoreView.setText("X Score: "+ ++xScore);
                return 1; // player x has won
            } else {
                information_view.setText(R.string.o_won);
                Log.d("checks", "o won: Diagonal");
                oScoreView.setText("O Score: "+ ++oScore);
                return 2;
            } // else
        } else if (9 == plays && !gameOver) { // 9 plays have been done and no winner yet-DRAW
            information_view.setText(R.string.draw);
            Log.d("checks", "there was a draw");
            return 3;
        } else {
            Log.d("checks", "no winner found");
            return 0; // no winner yet
        } // else
    } // checkWinner

    /*
    * showGameResetDialog
    * parameters: int _win_val - int representation of winner
    * returns: none
    * shows an alert depicting winner or a draw
    */
    private void showGameResetDialog(int _win_val) {
        //Ask the user if they want to quit
        AlertDialog.Builder alert = new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        resetGame();
                    } // onClick
                })
                .setNegativeButton(R.string.no, null);

        switch ( _win_val ) {
            case 1:
                alert.setTitle(R.string.x_won)
                    .setMessage(R.string.replay_x_won);
                break;
            case 2:
                alert.setTitle(R.string.o_won)
                        .setMessage(R.string.replay_o_won);
                break;
            case 3:
                alert.setTitle(R.string.draw)
                        .setMessage(R.string.replay_draw);
                break;
        } // switch

        alert.show();

    } // showGameResetDialog

    /* resetGame
    * parameters: none
    * returns: none
    * resets game to its default state
    */
    private void resetGame() {
        Log.d("click", "New Game Clicked");
        information_view.setText(R.string.x_turn);
        plays = 0;
        gameOver = false;
        xTurn = true;
        // reset all backgroundResources to nothing and make buttons clickable
        for (int i = 0; i < 9; i++) {
            cells[i].setBackgroundResource(R.drawable.none);
            cells[i].setClickable(true);
            cells[i].setTag(""+i); // ""+i to make them unique
        } // for
        Log.d("action", "Game Reset");
    } // resetGame

    /*
    * endGame
    * parameters: none
    * returns: int
    * sets the game to an end state
    */
    private void endGame() {
        gameOver = true;
        // iterate through cells and disable all buttons
        for (int i = 0; i < 9; ++i) {
            cells[i].setClickable(false);
        } // if
    } // endGame

} // MainActivity
